//Теоретичні питання
// 1. Які існують типи даних у Javascript?
// Стандарт ECMAScript визначає 8 типів даних: 6 примітивних типів: Undefined (Невизначений тип), Boolean (Булеві, Логічний тип), Number (Число), BigInt, String (рядок), Symbol (в ECMAScript 6).
// А також Null (Null тип) і Object (Об`єкт).
// 2. У чому різниця між == і ===?
// Це оператори рівності, однок оператор суворої рівності (===) перевіряє рівність без приведення типів, а при нестрогій рівності (==) операнди різних типів перетворюються оператором == до числа.
// 3. Що таке оператор?
// Оператор - це інструкція мови програмування, якою задається певний крок процесу обробки інформації. Оператори служать для керування потоком команд в JavaScript.
// Оператори існують 3-х видів: унарні, бінарні та тернарні.
// У мові JavaScript існуе 8 класів операторів: арифметичні, побітові, логічні, оператори порівняння, рядкові, оператори присвоєння, умовні, додаткові.


let userName = prompt("What is your name?").trim();
while (!userName) {
    alert("Sorry, but you didn`t write your name. Try again.")
    userName = prompt("What is your name?").trim();
}
let age = prompt("How old are you?").trim();
while(!age || age === undefined || age === "" || age === null) {
    alert("Sorry, but you didn`t write your age. Try again.")
    age = prompt("How old are you?").trim();
}
if (age <18) {
    alert("You are not allowed to visit this website");
} else if (age >=18 || age <=22) {
    let answer = confirm ("Are you sure you want to continue?");
    if (answer === true) {
     alert("Welcome, " +userName);
 } else if (answer === false) {
     alert("You are not allowed to visit this website;");
 }
 } else {
    alert("Welcome, " +userName);
}